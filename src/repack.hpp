#pragma once
#include <string>

namespace dlc {
extern auto repack(const std::string& input,
                   const std::string& output,
                   const std::string& rpath = {}) -> int;
}; /// namespace dlc
