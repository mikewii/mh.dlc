#include "utils.hpp"

#include <libmh/tools/istream.hpp>
#include <libmh/crypto/pk.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>

namespace dlc {
extern bool fVerbose;

auto generateRSA(void) -> int
{
    using namespace mh::crypto;

    static std::string names[] = {
        "outer", "inner"
    };

    for (const auto& name : names) {
        std::string privName = name + "_priv.rsa.der";
        std::string pubName = name + "_pub.rsa.der";
        std::ofstream priv(privName, std::ios::out | std::ios::binary);
        std::ofstream pub(pubName, std::ios::out | std::ios::binary);
        PK pk;

        if (!priv.is_open())
            return printError("Failed to open file for write: " + privName), 1;

        if (!pub.is_open())
            return printError("Failed to open file for write: " + pubName), 1;

        if (!pk.generateRSA())
            return printError("Failed to generate RSA"), 1;

        if (fVerbose)
            std::cout << "Writing RSA private key: " << privName << std::endl;

        if (!pk.writePrivateKey(priv))
            return printError("Failed to write RSA private key"), 1;

        if (fVerbose)
            std::cout << "Writing RSA public key: " << pubName << std::endl;

        if (!pk.writePublicKey(pub))
            return printError("Failed to write RSA public key"), 1;
    }

    return 0;
}

auto printError(const std::error_code& ec) -> bool
{
    std::cerr << ec.message() << std::endl;
    return false;
}

auto printError(const std::string& str) -> bool
{
    std::cerr << str << std::endl;
    return false;
}

auto isExist(const std::string& path) noexcept -> bool
{
    std::error_code ec;

    return std::filesystem::exists(path, ec);
}

auto isFile(const std::string& path) noexcept -> bool
{
    std::error_code ec;

    if (!std::filesystem::is_regular_file(path, ec)) {
        if (ec)
            return printError(ec);

        return false;
    }

    return true;
}

auto isDir(const std::string& path) noexcept -> bool
{
    std::error_code ec;

    return std::filesystem::is_directory(path, ec);
}

auto readKey(const std::string& path, std::vector<u8>& key) -> bool
{
    std::ifstream in(path, std::ios::in | std::ios::binary);

    if (!in.is_open())
        return false;

    auto size = mh::tools::istream::size(in);

    if (size <= 0)
        return false;

    key.resize(size);

    mh::tools::istream::read(in, key.data(), key.size());

    return true;
}

auto print(const std::string& str) -> void
{
    std::cout << str << std::endl;
}

auto makeOutputPath(std::string& out,
                    const std::string& input,
                    const std::string& output,
                    const std::string& rpath) -> bool
{
    std::filesystem::path path;

    if (isDir(output)) {
        path /= output;

        if (rpath.empty())
            path /= std::filesystem::path(input).filename();
    } else {
        path /= output;
    }

    out = path.string();

    return path.empty() == false;
}

auto makeOutputDir(std::string& out,
                   const std::string& input,
                   const std::string& output,
                   const std::string& rpath) -> bool
{
    std::filesystem::path path;

    if (isDir(output))
        path /= output;

    if (rpath.empty()) {
        path /= std::filesystem::path(input).filename();
    } else {
        auto relative = std::filesystem::relative(input, rpath);
        path /= relative;

        if (relative.has_parent_path()) {
            auto copy = path;

            copy.remove_filename();

            std::error_code ec;
            std::filesystem::create_directories(copy, ec);

            if (ec)
                return printError(ec);
        }
    }

    out = path.string();

    return path.empty() == false;
}

auto makeDirs(const std::string& path) noexcept -> bool
{
    std::error_code ec;
    std::filesystem::create_directories(path, ec);

    if (ec)
        return printError(ec);

    return true;
}

template auto walkExecute<std::filesystem::directory_iterator>(Fun&& fun,
                                                               const std::string& input,
                                                               const std::string& output) -> int;

template auto walkExecute<std::filesystem::recursive_directory_iterator>(Fun&& fun,
                                                                         const std::string& input,
                                                                         const std::string& output) -> int;

template <typename IT>
auto walkExecute(Fun&& fun,
                 const std::string& input,
                 const std::string& output) -> int
{
    if (!makeDirs(output))
        return 1;

    std::error_code ec;
    std::filesystem::directory_options options = std::filesystem::directory_options::skip_permission_denied;
    IT it(input, options, ec);
    std::filesystem::path absolute = std::filesystem::absolute(output).lexically_normal();

    for (const auto& entry : it) {
        if (entry.is_regular_file(ec)) {
            if (ec)
                return printError(ec), 1;

            auto abs = std::filesystem::absolute(entry.path().lexically_normal());

            if (abs.string().find(absolute.string()) != std::string::npos)
                continue;

            if (fun(entry.path().string(), absolute.string(), input) != 0)
                return 1;
        }
    }

    return 0;
}

}; /// namespace dlc
