#include "unpack.hpp"
#include "utils.hpp"

#include <libmh/MH/CryptoX.hpp>
#include <libmh/MH/SignX.hpp>
#include <psa/crypto.h>

#include <fstream>
#include <filesystem>

namespace dlc {
constexpr std::size_t minSize = (RSA_SIGNATURE_SIZE * 2) + sizeof(u32) + PSA_HASH_LENGTH(PSA_ALG_SHA_1);
extern bool fVerbose;
extern bool fNoVerify;
extern std::vector<u8> innerPubKey;
extern std::vector<u8> outerPubKey;
extern std::string blowfishKey;
extern mh::crypto::Key::e region;

auto checkSize(const std::string& path) -> int
{
    std::error_code ec;
    auto size = std::filesystem::file_size(path, ec);

    if (ec)
        return printError(ec), 1;

    if (size <= minSize)
        if (fVerbose)
            print("File size too small, skipping: " + path);

    return 0;
}

auto unpack(const std::string& input,
            const std::string& output,
            const std::string& rpath) -> int
{
    if (checkSize(input) != 0)
        return 1;

    std::string outpath;
    auto res = rpath.empty() ? makeOutputPath(outpath, input, output, rpath)
                             : makeOutputDir(outpath, input, output, rpath);

    if (!res)
        return printError("Failed to get output path for " + input), 1;

    std::ifstream in(input, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::binary | std::ios::trunc);

    if (!in.is_open() || !out.is_open())
        return 1;

    mh::crypto::CryptoX crypto(in, out);

    if (fVerbose) {
        auto path = rpath.empty() ? std::filesystem::path(input).filename().string()
                                  : std::filesystem::relative(input, rpath).string();

        print("Decrypting: " + path + ' ' + outpath);
    }

    /// Verify
    if (!fNoVerify) {
        mh::crypto::SignX sign;
        auto key = mh::crypto::Key::getRSAPublicKey(region);

        if (!outerPubKey.empty()) {
            if (!sign.verify(in, outerPubKey, RSA_SIGNATURE_SIZE))
                return printError("Verify outer signature failed: " + input), 1;
        } else {
            if (!sign.verify(in, key.first, key.second, RSA_SIGNATURE_SIZE))
                return printError("Verify outer signature failed: " + input), 1;
        }

        if (!innerPubKey.empty()) {
            if (!sign.verify(in, innerPubKey, RSA_SIGNATURE_SIZE * 2))
                return printError("Verify inner signature failed: " + input), 1;
        }
    }

    /// Decrypt
    if (!blowfishKey.empty()) {
        if (!crypto.decrypt(  reinterpret_cast<const u8*>(blowfishKey.data())
                            , blowfishKey.size()))
            return printError("Failed to decrypt: " + input), 1;

    } else if (!crypto.decryptDLC(region)) {
        return printError("Failed to decrypt: " + input), 1;
    }

    return 0;
}
}; /// namespace dlc
