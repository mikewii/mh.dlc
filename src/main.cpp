#include "utils.hpp"
#include "unpack.hpp"
#include "repack.hpp"

#include <libmh/MH/Keys.hpp>

#include <cxxopts.hpp>

#include <iostream>
#include <filesystem>

namespace dlc {
bool fVerbose{false};
bool fNoVerify{false};
std::vector<u8> innerPrivKey;
std::vector<u8> outerPrivKey;
std::vector<u8> innerPubKey;
std::vector<u8> outerPubKey;
std::string blowfishKey;
mh::crypto::Key::e region = mh::crypto::Key::e::NONE;
}; /// namespace dlc

std::string innerPubKeyStr, innerPrivKeyStr;
std::string outerPubKeyStr, outerPrivKeyStr;

auto readKeys(void) -> bool
{
    using namespace dlc;

    /// Public
    if (!outerPubKeyStr.empty() && outerPubKeyStr != "internal")
        if (!readKey(outerPubKeyStr, outerPubKey))
            return printError("Failer to read outer pubkey " + outerPubKeyStr);

    if (!innerPubKeyStr.empty())
        if (!readKey(innerPubKeyStr, innerPubKey))
            return printError("Failer to read inner pubkey " + innerPubKeyStr);

    /// Private:
    if (!outerPrivKeyStr.empty())
        if (!readKey(outerPrivKeyStr, outerPrivKey))
            return printError("Failer to read outer privkey " + outerPrivKeyStr);

    if (!innerPrivKeyStr.empty())
        if (!readKey(innerPrivKeyStr, innerPrivKey))
            return printError("Failer to read inner privkey " + innerPrivKeyStr);

    return true;
}

auto switchRegion(const std::string& region) -> bool
{
    if (region == "JP")
        dlc::region = mh::crypto::Key::e::MHX_DLC_JP;
    else if (region == "US")
        dlc::region = mh::crypto::Key::e::MHX_DLC_US;
    else if (region == "EU")
        dlc::region = mh::crypto::Key::e::MHX_DLC_EU;
    else if (region == "TW")
        dlc::region = mh::crypto::Key::e::MHX_DLC_TW;

    if (dlc::region == mh::crypto::Key::e::NONE)
        return dlc::printError("Region not supported: " + region);

    return true;
}

auto main(int argc, char* argv[]) -> int
{
    using namespace dlc;

    cxxopts::Options options("mh.dlc", "Unpack/Repack MHGEN/MHX dlc files");
    std::string in, out, region;
    bool fLoad{false};
    bool fGenerate{false};
    bool fEncrypt{false};
    bool fDecrypt{true};
    bool fNoVerify{false};
    bool fRecursive{false};

    options.add_options()
        ("b,blowfish", "Blowfish key to use (4 - 56 characters)", cxxopts::value<std::string>(blowfishKey)->default_value("internal"))

        ("d,decrypt", "Decrypt", cxxopts::value<bool>(fDecrypt)->default_value("true"))
        ("e,encrypt", "Encrypt", cxxopts::value<bool>(fEncrypt)->default_value("false"))

        ("g,generate", "Generate 2 RSA key pairs required for signing and exit", cxxopts::value<bool>(fGenerate)->default_value("false"))

        ("n,noverify", "Dont verify signatures", cxxopts::value<bool>(fNoVerify)->default_value("false"))

        ("l,load", "Load \"[inner|outer]_[priv|pub].rsa.der\" RSA keys from root folder", cxxopts::value<bool>(fLoad)->default_value("false"))

        ("i,input", "Input path", cxxopts::value<std::string>(in))
        ("o,output", "Output path", cxxopts::value<std::string>(out))

        ("R,recursive", "Recursively process input dir", cxxopts::value<bool>(fRecursive)->default_value("false"))
        ("r,region", "Game region: JP|US|EU|TW", cxxopts::value<std::string>(region)->default_value("JP"))

        ("pubo", "Outer RSA pubkey path|internal", cxxopts::value<std::string>(outerPubKeyStr)->default_value("internal"))
        ("pubi", "Inner RSA pubkey path", cxxopts::value<std::string>(innerPubKeyStr))
        ("privo", "Outer RSA privkey path", cxxopts::value<std::string>(outerPrivKeyStr))
        ("privi", "Inner RSA privkey path", cxxopts::value<std::string>(innerPrivKeyStr))

        ("v,verbose", "Verbose output", cxxopts::value<bool>(fVerbose)->default_value("false"))
        ("h,help", "Print help and exit.");

    try {
        auto result = options.parse(argc, argv);

        if (result.count("help")) {
            std::cout << options.help() << std::endl;

            return 0;
        }
    } catch (const std::exception& e) {
        std::cerr << "Error parsing options: " << e.what() << std::endl;
        return 1;
    }

    if (fGenerate)
        return generateRSA();

    if (fLoad) {
        outerPubKeyStr = "outer_pub.rsa.der";
        outerPrivKeyStr = "outer_priv.rsa.der";
        innerPubKeyStr = "inner_pub.rsa.der";
        innerPrivKeyStr = "inner_priv.rsa.der";
    }

    if (!readKeys())
        return 1;

    if (!switchRegion(region))
        return 1;

    if (fEncrypt) {
        if (fVerbose)
            print("::Encrypt::");

        if (isDir(in)) {
            if (fRecursive)
                return walkExecute<std::filesystem::recursive_directory_iterator>(repack, in, out);
            else
                return walkExecute<std::filesystem::directory_iterator>(repack, in, out);
        } else {
            return repack(in, out);
        }
    } else if (fDecrypt) {
        if (fVerbose)
            print("::Decrypt::");

        if (isDir(in)) {
            if (fRecursive)
                return walkExecute<std::filesystem::recursive_directory_iterator>(unpack, in, out);
            else
                return walkExecute<std::filesystem::directory_iterator>(unpack, in, out);
        } else {
            return unpack(in, out);
        }
    }

    return 0;
}
