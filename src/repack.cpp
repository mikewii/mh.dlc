#include "repack.hpp"
#include "utils.hpp"

#include <libmh/MH/CryptoX.hpp>
#include <libmh/MH/SignX.hpp>
#include <libmh/tools/ostream.hpp>

#include <fstream>
#include <filesystem>

namespace dlc {
extern bool fVerbose;
extern std::vector<u8> innerPrivKey;
extern std::vector<u8> outerPrivKey;
extern std::string blowfishKey;
extern mh::crypto::Key::e region;

auto repack(const std::string& input,
            const std::string& output,
            const std::string& rpath) -> int
{
    std::string outpath;
    auto res = rpath.empty() ? makeOutputPath(outpath, input, output, rpath)
                             : makeOutputDir(outpath, input, output, rpath);

    if (!res)
        return printError("Failed to get output path for " + input), 1;

    std::ifstream in(input, std::ios::in | std::ios::binary);
    std::fstream out(outpath, std::ios::in | std::ios::out | std::ios::binary | std::ios::trunc);

    if (!in.is_open() || !out.is_open())
        return 1;

    mh::crypto::CryptoX crypto(in, out);
    mh::crypto::SignX sign;

    if (fVerbose) {
        auto path = rpath.empty() ? std::filesystem::path(input).filename().string()
                                  : std::filesystem::relative(input, rpath).string();

        print("Encrypting: " + path + ' ' + outpath);
    }

    /// Encrypt
    if (!blowfishKey.empty()) {
        if (!crypto.encrypt(  reinterpret_cast<const u8*>(blowfishKey.data())
                            , blowfishKey.size()))
            return printError("Failed to decrypt: " + input), 1;

    } else if (!crypto.encryptDLC(region)) {
        return printError("Failed to encrypt: " + input), 1;
    }

    /// Sign
    if (!outerPrivKey.empty() && !innerPrivKey.empty()) {
        if (!sign.sign(out, innerPrivKey))
            return printError("Inner key failed to sign: " + input), 1;

        if (!sign.sign(out, outerPrivKey))
            return printError("Outer key failed to sign: " + input), 1;
    } else {
        mh::tools::ostream::fill(out, RSA_SIGNATURE_SIZE * 2);
    }

    return 0;
}
}; /// namespace dlc
