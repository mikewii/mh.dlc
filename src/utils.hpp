#pragma once
#include <libmh/types.hpp>
#include <system_error>
#include <vector>
#include <string>
#include <functional>

namespace dlc {
using Fun = std::function<int(const std::string& input,
                              const std::string& output,
                              const std::string& rpath)>;

template <typename IT>
extern auto walkExecute(Fun&& fun,
                        const std::string& input,
                        const std::string& output) -> int;

extern auto generateRSA(void) -> int;
extern auto print(const std::string& str) -> void;
extern auto printError(const std::error_code& ec) -> bool;
extern auto printError(const std::string& str) -> bool;
extern auto isExist(const std::string& path) noexcept -> bool;
extern auto isFile(const std::string& path) noexcept -> bool;
extern auto isDir(const std::string& path) noexcept -> bool;
extern auto makeDirs(const std::string& path) noexcept -> bool;
extern auto readKey(const std::string& path, std::vector<u8>& key) -> bool;
extern auto setRegion(const std::string& region) -> bool;

extern auto makeOutputPath(std::string& out,
                           const std::string& input,
                           const std::string& output,
                           const std::string& rpath = {}) -> bool;

extern auto makeOutputDir(std::string& out,
                          const std::string& input,
                          const std::string& output,
                          const std::string& rpath = {}) -> bool;
}; /// namespace dlc
